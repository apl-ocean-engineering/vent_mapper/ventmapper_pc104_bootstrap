# Ventmapper Linux VM Ansible bootstrap

Ansible scripts and etc for bootstrapping a fresh Ubuntu 22.04 server install for the ventmapper datapod.

Note that these scripts assume the non-root user and hostname are set during OS install and do not need to be set by Ansible.

The following instructions assume you are running as a non-root user (typically "sysop") with sudo permissions.

## Initial bootstrap

When bootstrapping from a clean installation, as root, run:

```
wget -O- https://gitlab.com/apl-ocean-engineering/vent_mapper/ventmapper_pc104_bootstrap/-/raw/dev/adapt_to_linux_vm/linux_vm/bootstrap_linux_vm.sh?ref_type=heads | SYSOP_PASSWORD=somepassword /bin/bash
```

This will create the user "sysop" with the specified password.   On subequent runs ...

## Subsequent runs

Once bootstrapped, the script can be run repeatedly by any user on the system from within the local git checkout at `~/ventmapper_pc104_bootstrap/linux_vm/run_linux_vm.sh`
