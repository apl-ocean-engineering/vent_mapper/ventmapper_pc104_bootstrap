#!/bin/bash

ANSIBLE_SECRETS_FILE=$HOME/.ansible-secrets.yaml
ANSIBLE_FILE=linux_vm_ansible.yml

if [ -f $ANSIBLE_SECRETS_FILE ]; then
    echo "Sourcing secrets from $ANSIBLE_SECRETS_FILE"
    SECRETS_ARGS=-e "@$ANSIBLE_SECRETS_FILE"
fi

sudo ansible-playbook $SECRETS_ARGS \
        -vvv $ANSIBLE_FILE
