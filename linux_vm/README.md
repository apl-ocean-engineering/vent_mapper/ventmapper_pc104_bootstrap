# Ventmapper Linux VM Ansible bootstrap

Ansible scripts and etc for bootstrapping a fresh Ubuntu 22.04 lxc container install for the ventmapper datapod.

## Initial bootstrap

Starting from scratch, the container does not start with a non-root user.  "Bootstrapping" this repo will
create an initial user "sysop" and configure the system.

```
wget -O- https://gitlab.com/apl-ocean-engineering/vent_mapper/ventmapper_pc104_bootstrap/-/raw/dev/adapt_to_linux_vm/linux_vm/bootstrap_linux_vm.sh?ref_type=heads | SYSOP_PASSWORD=somepassword /bin/bash
```

## Subsequent runs

Once bootstrapped, the script can be run from within the local git checkout:

```
run_local.sh
```
