#!/bin/bash

set -e

if [ "$SYSOP_PASSWORD" == "" ]; then
    echo "Environment variable SYSOP_PASSWORD must be set"
    exit -1
fi

# Need software-properties-common got add-apt-repository
sudo apt-get update
sudo apt-get install -y --no-install-recommends \
        git \
        gpg-agent \
        software-properties-common

sudo add-apt-repository universe
sudo apt-get update
sudo add-apt-repository --yes --update ppa:ansible/ansible
sudo apt-get install -y -f --no-install-recommends ansible

sudo ansible-pull -vvv \
            -C dev/adapt_to_linux_vm \
            --extra-vars "var_password=$SYSOP_PASSWORD" \
            -U https://gitlab.com/apl-ocean-engineering/vent_mapper/ventmapper_pc104_bootstrap.git \
            linux_vm/linux_vm_ansible.yml
